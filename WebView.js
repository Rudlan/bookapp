import React from 'react';
import { WebView } from 'react-native-webview';
import styles from './Styles.js';

export default  class WebViewScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Acheter",
      headerStyle: {
        backgroundColor: '#24a0ed',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    };
  };
    render() {
      return (
        <WebView
        source={{ uri: this.props.navigation.state.params.url }}
        style={{ marginTop: 10 }}
      />
        
      );
    }
  }