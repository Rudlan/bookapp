import React from 'react';
import { FlatList,View } from 'react-native';
import ItemList from './ItemList.js';
import { isEmpty }  from './function.js';
import styles from './Styles.js';
export default class ListBook extends React.Component{
    render(){
      return(
        <View style={styles.containerItem}>
          <FlatList data={this.props.data} renderItem={
            ({item}) => 
            {
              if (!isEmpty(item.volumeInfo.imageLinks)) {
                return <ItemList navigation={this.props.navigation} book= { item } name={item.volumeInfo.title} image = {item.volumeInfo.imageLinks.smallThumbnail} link={item.volumeInfo.previewLink} />
                
              }
              else{
                return <ItemList navigation={this.props.navigation} name={item.volumeInfo.title} link={item.volumeInfo.previewLink} publishedDate={ item.volumeInfo.publishedDate} 
            description = {item.volumeInfo.description}/>
              }
            }
            
            } keyExtractor={item => item.id}/>
        </View>
      )
    }
  }