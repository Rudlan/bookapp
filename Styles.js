
import { StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    containerItem:{
      flex:1,
      alignItems:"center",
      justifyContent:"center"
    },
    imgBook:{
      width:50,
      height:50,
    },
    centeredElement:{
      justifyContent:"center",
      alignItems:"center",
    },
    imgBookDetail:{
      width:250,
      height:250,
      marginTop:20,
    },
    loader:{
      flex:2,
      alignContent:"center",
      justifyContent:"center",
    },
    item:{
      padding:10,
      flex:1,
      flexDirection:"row",
    },
    mutedText:{
      fontSize:10,
    },
    btnAcheter:{
      position:"absolute",
      bottom:0,
    },
    title:{
      fontSize:25,
      marginTop:5,
      marginLeft:15,
      marginBottom:5,
    },
    btn:{
      marginTop:10,
    },
    list:{
      fontSize:17,
    },
    splashSceen:{
      backgroundColor:"#24a0ed",
    },
    splashSceenText:{
      color: 'white',
      fontSize: 40,
      fontWeight: 'bold'
    },
    p:{
      marginTop:10,
    },
    input:{
      height:40,
      borderColor: 'gray',
      borderWidth: 1,
    }
  });
export default styles;