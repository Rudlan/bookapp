import React from 'react';
import { Text,View } from 'react-native';
import styles from './Styles.js';
export default class SplashScreen extends React.Component {
    render() {
  
      return (
        <View style={[styles.container,styles.centeredElement,styles.splashSceen]}>
          <Text style={styles.splashSceenText}>
            BookApp
          </Text>
        </View>
      );
    }
  }