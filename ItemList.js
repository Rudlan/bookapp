import React from 'react';
import { Image, Text,View } from 'react-native';
import { isEmpty }  from './function.js';
import styles from './Styles.js';
export default class ItemList extends React.Component{
    
    render(){
      return(
        <View style={styles.item}>
          {isEmpty(this.props.image)?null:<Image source={{uri: this.props.image }}
         style={styles.imgBook} />}
          <Text style={styles.title} onPress={() => {this.props.navigation.navigate('Details',{book: this.props.book,title: this.props.name });}}>{ this.props.name }</Text>
        </View>
      )
    }
  }