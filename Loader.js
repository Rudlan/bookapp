import React from 'react';
import { ActivityIndicator,  View } from 'react-native';
import styles from './Styles.js';
export default class Loader extends React.Component{
    render(){
      return(
        <View style={styles.loader}> 
          <ActivityIndicator size="large"/>
        </View>
      )
    }
  }