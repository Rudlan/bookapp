import React from 'react';
import { Image,Text,View,Button } from 'react-native';
import { isEmpty }  from './function.js';
import styles from './Styles.js';
import { ScrollView } from 'react-native-gesture-handler';

export default class DetailsBookScreen extends React.Component{
      static navigationOptions = ({ navigation }) => {
        return {
          title: navigation.state.params.title,
          headerStyle: {
            backgroundColor: '#24a0ed',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        };
      };
    render(){
        const book = this.props.navigation.state.params.book;
        console.log(book);
        return(
            <ScrollView>
            <View style= { [styles.container, styles.centeredElement]}>
                
                {isEmpty(book.volumeInfo.imageLinks.smallThumbnail)?null:<Image source={{uri: book.volumeInfo.imageLinks.smallThumbnail }} style={styles.imgBookDetail} />}
                <Text style={styles.mutedText}>{book.volumeInfo.authors.length > 1?"Auteurs":"Auteur"} : { book.volumeInfo.authors.map(item => item)}</Text>
                <Text style={styles.mutedText}>Date de parution : {book.volumeInfo.publishedDate}</Text>
                <Text style={styles.p}>{ book.volumeInfo.description }</Text>
                <Button 
                    style={styles.btnAcheter}
                    title={"Acheter "+(isEmpty(book.saleInfo)?(""):(book?.saleInfo?.listPrice?.amount))+" "+(isEmpty(book?.saleInfo?.listPrice?.currencyCode)?(""):(book?.saleInfo?.listPrice?.currencyCode) )}
                    onPress={() => {this.props.navigation.navigate('PurchaseBook',{url: book.saleInfo.buyLink });}}
                />
            </View>
            </ScrollView>
        )
      }
}