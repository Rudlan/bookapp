import * as React from 'react';
import { Button, View, Text } from 'react-native';
import {
  createAppContainer
} from 'react-navigation';
import {
  createStackNavigator
} from 'react-navigation-stack';
import SplashScreen from './SplashScreen.js';
import HomeScreen from './HomeSceen.js';
import WebViewScreen from './WebView';
import DetailsBookScreen from './DetailsBook.js';


const RootStack = createStackNavigator({
    Home: {
      screen: HomeScreen
    },
    PurchaseBook:{
      screen :WebViewScreen,
    },
    Details:{
      screen: DetailsBookScreen,
    }
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {isLoading :true };
  }
  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    );
  }
  
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();
  
    if (data !== null) {
      this.setState({ isLoading: false });
    }
  }
  render() {
    if (this.state.isLoading) {
      return <SplashScreen />;
    }
    return <AppContainer />;
  }
}