import React from 'react';
import { TextInput, Button,  View } from 'react-native';
import Loader from './Loader.js';
import ListBook from './ListBook.js';
import {isEmpty} from './function.js';
import styles from './Styles.js';



export default class HomeScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {text: '',isLoad: true,dataSource: ""};
      }
      static navigationOptions = {
        title: 'Accueil',
        headerStyle: {
          backgroundColor: '#24a0ed',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      };
    searchBookByAuthors(text){
      if(!isEmpty(text)){
        return fetch(`https://www.googleapis.com/books/v1/volumes?q=inauthor:'${text}'&key=AIzaSyC6cGRj10uKMdOipMgUkgtQ-Z_p98JMyaA`)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoad: false,
              countItem: responseJson.totalItems,
              dataSource: responseJson.items,
            },function(){
    
            });
          })
          .catch((error) => {
            console.error(error);
          });
      } 
    }
    render(){
      return (
        <View style= { styles.container}>
          <TextInput style = { styles.input} placeholder="Rechercher un auteur" onChangeText= { text => {if(text==""){this.setState({isLoad: true})}else{this.setState({text});}}}></TextInput>
          <Button
            title="Rechercher"
            onPress={() => this.searchBookByAuthors(this.state.text)}
          />
          {this.state.isLoad?
          (
            <Loader></Loader>
          ):(
            <ListBook navigation={this.props.navigation} data={this.state.dataSource}></ListBook>
          )
          }
  
        </View>
      )
    }
  }